addSbtPlugin("com.thesamet"           % "sbt-protoc"            % "0.99.18")

// Workaround for failed dependency download for lightbend-commercial repo
updateOptions := updateOptions.value.withGigahorse(false)

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.7.1"
